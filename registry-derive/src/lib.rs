extern crate proc_macro;
#[macro_use]
extern crate quote;
extern crate syn;
#[macro_use]
extern crate synstructure;

use proc_macro::TokenStream;

const DEFAULT_VALUE_KEY: &'static str = "default";

#[proc_macro_derive(FromConfig, attributes(default))]
pub fn from_config(input: TokenStream) -> TokenStream {
    // Construct a string representation of the type definition
    let s = input.to_string();

    // Parse the string representation
    let ast = syn::parse_str(&s).unwrap();

    // Build the impl
    let gen = impl_from_config(&ast, false);

    // Return the generated impl
    gen.into()
}

#[proc_macro_derive(FromConfigLocal, attributes(default))]
pub fn from_config_local(input: TokenStream) -> TokenStream {
    // Construct a string representation of the type definition
    let s = input.to_string();

    // Parse the string representation
    let ast = syn::parse_str(&s).unwrap();

    // Build the impl
    let gen = impl_from_config(&ast, true);

    // Return the generated impl
    gen.into()
}

fn impl_from_config(ast: &syn::DeriveInput, is_local: bool) -> quote::Tokens {
    let s = synstructure::Structure::new(ast);
    let name = &ast.ident;
    if let syn::Data::Struct(ref st) = ast.data {
        if let syn::Fields::Named(ref fields) = st.fields {
            let field_names = fields.named.iter().map(|f| f.ident);
            let field_value_impls = fields.named.iter().map(|f| {
                let field_name = f.ident.expect("unnamed fields are not allowed");
                let default_value = f.attrs.iter().filter_map(|a| {
                    if let Some(syn::Meta::NameValue(ref nv)) = a.interpret_meta() {
                        if nv.ident.as_ref() == DEFAULT_VALUE_KEY {
                            if let syn::Lit::Str(ref s) = nv.lit {
                                return Some(s.value());
                            } else {
                                panic!("{} on {} must be a &str", DEFAULT_VALUE_KEY, field_name.as_ref());
                            }
                        }
                    }
                    return None;
                }).next();
                if let Some(default_value) = default_value {
                    quote! {
                        ::std::str::FromStr::from_str(conf_values.get(stringify!(#field_name)).map(|s| s.as_str()).unwrap_or(#default_value)).unwrap()
                    }
                } else {
                    quote! {
                        ::std::str::FromStr::from_str(conf_values.get(stringify!(#field_name)).unwrap()).unwrap()
                    }
                }
            });
            let trait_ident = if is_local {
                quote!(::FromConfig)
            } else {
                quote!(::registry::FromConfig)
            };
            s.unbound_impl(trait_ident, quote! {
                fn from_config(conf_values: &::std::collections::HashMap<String, String>) -> ::std::result::Result<Self, ::failure::Error> {
                    Ok(#name {
                        #(#field_names: #field_value_impls),*
                    })
                }
            })
        } else {
            panic!("unnamed structs are not supported");
        }
    } else {
        panic!("non-struct types (ie. enums) are not supported");
    }
}
