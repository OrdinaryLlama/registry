#[macro_use]
extern crate diesel;
#[macro_use]
extern crate failure;
#[macro_use]
extern crate registry_derive;
#[macro_use]
extern crate slog;

#[doc(hidden)]
pub use registry_derive::*;

mod schema;

#[cfg(test)]
mod test;

use std::collections::HashMap;
use std::io::Error as IoError;
use std::net::{ToSocketAddrs,SocketAddr};
use std::result::Result;

use diesel::prelude::*;
use failure::Error;
use slog::Logger;

use ::schema::registry as db;

struct HostnameAndPort {
    hostname: String,
    port: u16,
}

impl HostnameAndPort {
    fn to_socket_addr(&self) -> Result<SocketAddr, Error> {
        let mut addrs = self.to_socket_addrs()?;
        match addrs.next() {
            None => Err(format_err!("dns lookup failed for {}", &self.hostname)),
            Some(addr) => Ok(addr),
        }
    }
}

impl ToSocketAddrs for HostnameAndPort {
    type Iter = std::vec::IntoIter<SocketAddr>;

    fn to_socket_addrs(&self) -> Result<Self::Iter, IoError> {
        (self.hostname.as_str(), self.port).to_socket_addrs()
    }
}

#[derive(Queryable)]
struct ConfigEntry {
    section: String,
    conf_key: String,
    conf_value: String,
}

#[derive(Queryable)]
struct SelfService {
    host: String,
    component: String,
    port: i32,
}

#[derive(Queryable)]
struct RemoteService {
    app_name: String,
    instance: String,
    host: String,
    component: String,
    port: i32,
}

pub struct Registry {
    app_name: String,
    instance: String,
    db_config: HashMap<String, HashMap<String, String>>,
    self_services: HashMap<String, HostnameAndPort>,
    remote_services: HashMap<(String, String), HashMap<String, HostnameAndPort>>,
    _logger: Logger,
}

impl Registry {
    // Note: this more genreal signature should work, but doesn't compile, I think it's a Diesel issue
    //pub fn load<C>(app_name: String, instance: String, db_conn: &C) -> Result<Self, Error>
    //where C: diesel::Connection, C::Backend: HasSqlType<Text> {
    pub fn load(app_name: String, instance: String, db_conn: &diesel::PgConnection, logger: Logger) -> Result<Self, Error> {
        info!(&logger, "registry: loading from database"; "app_name" => &app_name, "instance" => &instance);
        let mut db_config = HashMap::new();
        let mut self_services = HashMap::new();
        let mut remote_services = HashMap::new();

        for conf_entry in db::config::table.filter(
            db::config::app_name.eq(&app_name)
                .and(db::config::instance.eq(&instance)))
            .select((db::config::section, db::config::conf_key, db::config::conf_value))
            .load::<ConfigEntry>(db_conn)? {
            db_config.entry(conf_entry.section)
                .or_insert_with(|| HashMap::new())
                .insert(conf_entry.conf_key, conf_entry.conf_value);
        }

        for self_service in db::instances::table.find((&app_name, &instance))
            .inner_join(db::services::table.on(
                db::instances::app_name.eq(db::services::app_name).and(db::instances::instance.eq(db::services::instance))))
            .select((db::instances::host, db::services::component, db::services::port))
            .load::<SelfService>(db_conn)? {
            self_services.insert(self_service.component, HostnameAndPort {
                hostname: self_service.host,
                port: self_service.port as u16,
            });
        }

        for remote_service in db::instances::table
            .inner_join(db::services::table.on(
                db::instances::app_name.eq(db::services::app_name).and(db::instances::instance.eq(db::services::instance))))
            .select((db::instances::app_name, db::instances::instance, db::instances::host, db::services::component, db::services::port))
            .load::<RemoteService>(db_conn)? {
            remote_services.entry((remote_service.app_name, remote_service.instance))
                .or_insert_with(|| HashMap::new())
                .insert(remote_service.component, HostnameAndPort {
                    hostname: remote_service.host,
                    port: remote_service.port as u16,
                });
        }
        info!(&logger, "registry: loaded {} conf entries, {} self services, {} remote services",
            db_config.values().map(|m| m.len()).sum::<usize>(),
            self_services.len(),
            remote_services.values().map(|m| m.len()).sum::<usize>();
            "app_name" => &app_name, "instance" => &instance);

        Ok(Registry {
            app_name,
            instance,
            db_config,
            self_services,
            remote_services,
            _logger: logger,
        })
    }

    pub fn app_name(&self) -> &str {
        self.app_name.as_str()
    }

    pub fn instance(&self) -> &str {
        self.instance.as_str()
    }

    pub fn generate_config<T: FromConfig>(&self, section: &str) -> Result<T, Error> {
        match self.db_config.get(section) {
            None => T::from_config(&HashMap::new()),
            Some(entries) => T::from_config(entries),
        }
    }

    pub fn get_addr(&self, component: &str) -> Result<SocketAddr, Error> {
        match self.self_services.get(component) {
            None => Err(format_err!("no service definition found for {}.{}.{}", &self.app_name, &self.instance, component)),
            Some(host_and_port) => host_and_port.to_socket_addr(),
        }
    }

    pub fn get_remote_addr(&self, app_name: &str, instance: &str, component: &str) -> Result<SocketAddr, Error> {
        // this is far from ideal, due to the cloning, but it's good enough for now
        match self.remote_services.get(&(app_name.to_owned(), instance.to_owned()))
            .and_then(|components| components.get(component)) {
            None => Err(format_err!("no service definition found for {}.{}.{}", app_name, instance, component)),
            Some(host_and_port) => host_and_port.to_socket_addr(),
        }
    }
}

pub trait FromConfig: Sized {
    fn from_config(conf_values: &HashMap<String, String>) -> Result<Self, Error>;
}
