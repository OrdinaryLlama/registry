pub mod registry {
    table! {
        registry.config (app_name, instance, section, conf_key) {
            app_name -> Text,
            instance -> Text,
            section -> Text,
            conf_key -> Text,
            conf_value -> Text,
        }
    }

    table! {
        registry.hosts (hostname) {
            hostname -> Text,
        }
    }

    table! {
        registry.instances (app_name, instance) {
            app_name -> Text,
            instance -> Text,
            host -> Text,
        }
    }

    table! {
        registry.services (app_name, instance, component) {
            app_name -> Text,
            instance -> Text,
            component -> Text,
            port -> Int4,
        }
    }

    joinable!(instances -> hosts (host));

    allow_tables_to_appear_in_same_query!(
        config,
        hosts,
        instances,
        services,
    );
}
