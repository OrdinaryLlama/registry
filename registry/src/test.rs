use std::env;
use std::result::Result;

use diesel;
use diesel::prelude::*;
use failure::Error;
use slog;

use ::Registry;

#[derive(FromConfigLocal)]
struct TestConfig {
    foo: String,
    bar: i32,
    #[default = "qwerty"]
    wom: String,
    #[default = "123"]
    bat: i16,
    #[default = "456"]
    baz: i16,
}

#[test]
fn test_test1() {
    if let Err(e) = test_test1_impl() {
        panic!("error: {}", e);
    }
}

fn test_test1_impl() -> Result<(), Error> {
    let log = slog::Logger::root(slog::Discard, o!());
    let database_url = env::var("DATABASE_URL").map_err(|_| format_err!("DATABASE_URL must be set"))?;
    let db_conn = diesel::PgConnection::establish(&database_url)?;
    let registry = Registry::load("test".into(), "test1".into(), &db_conn, log)?;
    let my_config: TestConfig = registry.generate_config("config")?;
    assert_eq!(my_config.foo.as_str(), "abcdef");
    assert_eq!(my_config.bar, -9000);
    assert_eq!(my_config.wom.as_str(), "qwerty");
    assert_eq!(my_config.bat, 123);
    assert_eq!(my_config.baz, 42);
    Ok(())
}
